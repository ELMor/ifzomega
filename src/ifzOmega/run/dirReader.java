package ifzOmega.run;

import ifzOmega.run.DBConnection;
import ifzOmega.msg.reg.*;
import ifzOmega.msg.*;
import java.io.*;
import java.util.*;

/**
 * Se encarga de la lectura del directorio donde Omega genera los resultados,
 * Los lee e intruduce en la BBDD
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

public class dirReader extends Thread {
  DBConnection conn=null;
  String dirw=null;
  public dirReader(int opt[], String dbtype, String surl, String login, String pass,String dw) {
    try {
      conn=new DBConnection(opt,dbtype,surl,login,pass);
      dirw=dw;
    }catch(Exception e){
      e.printStackTrace();
      System.exit(-1);
    }
  }

  /**
   * Monitoriza cada 30 segundos el directorio y procesa los mensajes entrantes
   * que son ficheros cuya extension es '.res' (minusculas). Una vez procesados
   * les coloca el sufijo '.res.processed'
   *
   */
  public void run()
  {
    try{
      for(;;){
        File dir=new File(dirw);
        File files[]=dir.listFiles();
        for(int i=0;i<files.length;i++){
          if( files[i].getName().endsWith(".res") )
            processResponse(files[i]);
        }
        Thread.sleep(30000);
      }
    }catch(Exception e){
      e.printStackTrace();
      return;
    }
  }

  /**
   * Procesa fichero de Omega con resultados
   * @param f Nombre del fichero
   */
  private void processResponse(File f){
    try{
      FileInputStream fis=new FileInputStream(f);
      ASTMlexer l=new ASTMlexer(fis);
      ASTM p=new ASTM( l );
      Msg m=p.getMsg();
      fis.close();

      //Variables de lectura del mensaje de resultados
      String seqOmega=null;
      String seqPetic=null;
      for(Iterator i=m.getRegs().iterator();i.hasNext();){
        gen master=(gen)i.next();
        String result=null;
        String coment=null;
        switch (master.getHead().charAt(0)){
          case 'H':
            break;
          case 'P':
              seqOmega=master.getField(3); //Numero de peticion
            break;
          case 'O':
              seqPetic=master.getField(2); //Secuencia dentro de la peticion
              seqOmega=master.getField(3); //Numero de peticion
            break;
          case 'R':
              result=master.getField(4);
            break;
          case 'M':
              result=master.getField(3)+":"+master.getField(4);
            break;
          case 'C':
              coment=master.getField(4);
            break;
          case 'L':
            break;
        }
        if(result!=null){ //Grabar resultado en seqOmega,seqPetic
          conn.setResult(seqOmega,seqPetic,result);
        }
        if(coment!=null){ //Grabar comentario en seqOmega,seqPetic
          conn.setComments(seqOmega,seqPetic,coment);
        }
      }
      f.renameTo( new File(f.getName()+".processed") );
    }catch(Exception e){
      System.err.println(f.getName());
      f.renameTo( new File(f.getAbsolutePath()+".error") );
      e.printStackTrace();
    }
  }

}
