package ifzOmega.run;
import java.sql.*;
import java.util.*;
import java.text.*;

/**
 * Clase que maneja toda la conexion con BBDD del producto
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

public class DBConnection {
  private Connection conn=null; //Conexi�n de BBDD
  private String rdbms=null;
  int debug;

  /**
   * Se conecta a Oracle, SQLServer, Sybase ASE/ASA o a Informix
   * @param opt    Indica modo depuracion
   * @param dbtype ["oracle"|"sqlserver"|"sybase"|"informix"]
   * @param surl   La URL de conexion, depende de cada motor
   * @param login  Usuario
   * @param pass  Passwords
   * @throws Exception
   */
  public DBConnection(int opt[], String dbtype, String surl, String login, String pass)
      throws Exception
  {
    Properties props = new Properties();
    props.put("user"    , login);
    props.put("password", pass);
    if( dbtype.equalsIgnoreCase("oracle") ){
      //Oracle 9i
      Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
      surl="jdbc:oracle:thin:@"+surl;
      rdbms="oracle";
    }else if( dbtype.equalsIgnoreCase("sqlserver") ){
      //Dos opciones: el controlador de Weblogic o el de Microsoft
      //Weblogic:
      surl="jdbc:weblogic:mssqlserver4:"+surl;
      Class.forName("weblogic.jdbc.mssqlserver4.Driver").newInstance();
      //Microsoft
   /*
   surl="jdbc:microsoft:sqlserver://"+surl;
   Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
   */
      rdbms="sqlserver";
    }else if( dbtype.equalsIgnoreCase("sybase") ){
      //Sybase ASE, ASA
      surl="jdbc:sybase:Tds:"+surl;
      props.put("USE_METADATA","false");
      Class.forName("com.sybase.jdbc2.jdbc.SybDriver").newInstance();
      rdbms="sybase";
    }else if( dbtype.equalsIgnoreCase("informix") ){
      //Informix Dynamic Server
      surl="jdbc:weblogic:informix4:"+surl;
      Class.forName("weblogic.jdbc.informix4.Driver").newInstance();
      rdbms="informix";
    }else{
      System.out.println("BD:"+dbtype+" No encontrada");
      throw new Exception(dbtype+" Not found");
    }
    conn=DriverManager.getConnection(surl,props);
  }

  /**
   * Devuelve la fecha de ahora en distintos motores
   * @return la fecha de ahora segun el motor
   */
  private String sqlNow()
  {
    if( rdbms.equals("oracle") )
      return "sysdate";
    if( rdbms.equals("sybase") )
      return "getdate()";
    if( rdbms.equals("sqlserver") )
      return "getdate()";
    return "(no se el formato de today)";
  }

  /**
   * Retorna una nueva peticion para Omega2000.
   * Devuelve el max(inicializacion de hoy, max(ifzastm))
   *
   * @param ip Inicializacion diaria
   * @return  Retorna el siguiente PK a insertar
   * @throws SQLException
   */
  public String getFreshPet(String ip,String apk)
      throws SQLException
  {
    String sql1="Select max(ifz_seq_omega) from ifzastm,actividad_det "+
                "where actividad_det.actividad_pk="+apk+" and "+
                "ifzastm.actividad_det_pk=actividad_det.actividad_det_pk";
    Statement st1=conn.createStatement();
    ResultSet rs1=st1.executeQuery(sql1);
    if(rs1.next())
      if(rs1.getString(1)!=null)
        return rs1.getString(1);
    String sql="select max(ifz_seq_omega) from ifzastm";
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery(sql);
    int npk;
    if(rs.next()){
      npk=1+rs.getInt(1);
      if(rs.wasNull())
        npk=0;
    }else{
      npk=0;
    }
    SimpleDateFormat sdf=new SimpleDateFormat("MMdd");
    int nday=Integer.parseInt(sdf.format(new java.util.Date())+ip);
    if(nday>npk)
      return new Integer(nday).toString();
    return new Integer(npk).toString();
  }

  /**
   * Select para obtencion de las peticiones que han de ir ahora
   * Por ahora funciona con todos los motores.
   *
   * @return SELECT
   */
  private String sqlOrders(){
    return
    "select"+
        "	   ad.actividad_det_pk,"+
        "      c.codigo_cliente,"+
        "	   c.apellido1 apellido1,c.apellido2 apellido2 ,c.nombre nombre,"+
        "	   c.nac_fecha,"+
        "	   c.codigo_sexo,"+
        "	   a.codigo_personal cpd ,fp.apellido1 a1d ,fp.nombre nod ,fp.telefono ted,"+
        "	   a.codigo_servicio,s.servicio ,"+
        "	   t.tipo_episodio_desc ,"+
        "	   r.area,"+
        "	   s.cod_centro,n.nombre_centro,"+
        "	   p.prest_item_cod, p.prest_item_desc,"+
        "    a.actividad_pk "+
        "from"+
        "	 clientes 	   c,"+
        "	 actividad	   a,"+
        "	 actividad_det	ad,"+
        "	 servicios	   s,"+
        "	 prest_item	   p,"+
        "	 episodios	   e,"+
        "	 areas		   r,"+
        "	 tipo_episodio t,"+
        "	 centros	      n,"+
        "	 fpersona	   fp,"+
        "    ifzastm       i "+
        "where"+
        "     a.actividad_pk=ad.actividad_pk "+
        " and a.codigo_personal=fp.codigo_personal "+
        " and a.epis_pk=e.epis_pk "+
        " and e.codigo_cliente=c.codigo_cliente"+
        " and a.codigo_servicio=s.codigo_servicio"+
        " and s.codigo_area=r.codigo_area"+
        " and r.tipo_episodio_pk=t.tipo_episodio_pk"+
        " and s.cod_centro=n.cod_centro"+
        " and p.prest_item_pk=ad.prest_item_pk"+
        " and i.actividad_det_pk=ad.actividad_det_pk"+
        " and i.ifz_fec_envio is null "+
        " and a.actest_pk==2 "+
        "order by c.codigo_cliente,ad.actividad_det_pk";
  }

  public String getHC(String codigo_cliente)
      throws SQLException
  {
    //Retornamos el unico primary key reconocido por OpenSIC
    return codigo_cliente;
    /*
    String sql="select max(nhc) from hc were codigo_cliente="+codigo_cliente;
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery(sql);
    String hc=null;
    if(rs.next()){
      hc=rs.getString(1);
      if(rs.wasNull())
        hc=null;
    }
    return hc==null? "" : hc;
    */
  }

  public String getMuestra(String adpk)
      throws SQLException
  {
    String sql=
        "select pm.muestras_tipo_pk,mt.muestras_tipo_dec "+
        "from peticion_muestra pm,muestras_tipo mt "+
        "where pm.muestras_tipo_pk=mt.muestras_tipo_pk "+
        "  and pm.actividad_det_pk="+adpk;
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery(sql);
    String muestra=null;
    if(rs.next()){
      muestra=rs.getString(1)+"^"+rs.getString(2);
      if(rs.wasNull())
        muestra=null;
    }
    return muestra==null ? "" : muestra;
  }

  /**
   * Devuelve B o M segun se trate de Bioquimica o de Micro
   * ATENCION: Aun no implementado
   * @param picod es el prest_item_cod
   * @return B o M segun sea una prestacion de micro o de bioq
   */
  public String getBoM(String picod){
    /*
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery("");
    */
    return "B";
  }

  public ResultSet getOrders()
      throws SQLException
  {
    String sql=sqlOrders();
    Statement st=conn.createStatement();
    ResultSet ret=st.executeQuery(sql);
    return ret;
  }

  /**
   * Registra el cambio de estado de la tabla 'actividad'
   *
   * @param apk   actividad_pk
   * @param stat  Nuevos estado
   * @param ser   Servicio que realiza
   * @param prof  profesional que realiza
   * @throws SQLException
   */
  public void setActStatus(String apk, String stat, String ser, String prof)
      throws SQLException,Exception
  {
    Statement st=conn.createStatement();
    String sq=
        "insert into act_est_hist(esthist_pk,"
        +"actest_pk,actividad_pk,codigo_personal,codigo_servicio,"+
        "esthist_fecha,esthist_hora) values ("
        +getPK("act_est_hist")+","
        +stat+","+apk+","+prof+","+ser+","
        +sqlNow()+","+sqlNow()+")"
    ;
    st.executeUpdate(sq);
    st.executeUpdate("update actividad set actest_pk="+stat+" where actividad_pk="+apk);
    conn.commit();
  }

  /**
   * Marca la actividad como enviada en ifzastm
   *
   * @param adpk    Actividad_det_pk
   * @param seqOme  secuencia en Omega
   * @param seqPet  secuencia de la prestacion dentro de la secuancia de Omega
   * @throws SQLException
   */
  public void sendIfzAstm(String adpk, String seqOme, String seqPet)
      throws SQLException
  {
    Statement st=conn.createStatement();
    st.executeUpdate(
        "update ifzastm set ifz_seq_omega="+seqOme+",ifz_seq_pet="+seqPet
        +",ifz_fec_envio='"+new java.sql.Date( new java.util.Date().getTime() )+"'"
        +",ifz_senvio='OK'"
        +" where actividad_det_pk="+adpk);
    conn.commit();
  }

  public void setResult(String seqome, String seqpet, String result)
      throws SQLException,Exception
  {
    Statement st=conn.createStatement();
    String sql=
        "select actividad.actividad_pk,actividad_det.actividad_det_pk "
        +"from ifzastm,actividad,actividad_det "
        +"where ifzastm.ifz_seq_omega="+seqome
        +"  and ifzastm.ifz_seq_pet="+seqpet
        +"  and actividad_det.actividad_pk=actividad.actividad_pk "
        +"  and actividad_det.actividad_det_pk=ifzastm.actividad_det_pk";
    ResultSet rs=st.executeQuery(sql);

    String apk,adpk;
    if(  !rs.next()
         || (apk =rs.getString("actividad_pk"))==null
         || (adpk=rs.getString("actividad_det_pk"))==null
         )
    {
      throw new Exception(
          "No existe la (pet,seq) ("+seqome+","+seqpet+") en ifzastm"
          );
    }

    st.executeUpdate(
        "update actividad set "
        +" actdest_pk=4"
        +",act_frdo="+sqlNow()
        +",actividad_hora_rdo="+sqlNow()
        +" Where actividad_pk="+apk
        );
    //Se inserto ya el historico?
    rs=st.executeQuery(
        "select count(*) from act_est_hist where actividad_pk="+apk
        +" and actest_pk=4"
        );
    if( rs.next() ){
      int count=rs.getInt(1);
      if( count==0 ){
        st.executeUpdate(
            "insert into act_est_hist(actest_pk,actividad_pk,esthist_fecha,esthist_hora) values "
            +"(4,"+apk+","+sqlNow()+","+sqlNow()+")"
            );
      }
    }
    st.executeUpdate(
        "update actividad_det set "
        +",actividad_val_alf='"+result+"'"
        +" where actividad_det_pk="+adpk
        )  ;
  }

  public void setComments(String seqome, String seqpet, String comment)
      throws SQLException
  {
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery(
        "select actividad.actividad_pk,actividad_det.actividad_det_pk "
        +"from ifzastm,actividad,actividad_det "
        +"where ifzastm.ifz_seq_omega="+seqome
        +" and ifzastm.ifz_seq_pet="+seqpet
        +" and actividad_det.actividad_pk=actividad.actividad_pk "
        +" actividad_det.actividad_det_pk=ifzastm.actividad_det_pk"
        );
    String apk =rs.getString("actividad_pk");
    String adpk=rs.getString("actividad_det_pk");

    st.executeUpdate(
        "update actividad set "
        +" actividad_rdo_obs='"+comment+"'"
        +" Where actividad_pk="+apk
        );
  }

  /**
   * Obtiene el siguiente PK de la tabla de contadores
   * @param tn Nombre de la tabla
   * @return Siguiente PK
   * @throws SQLException
   * @throws Exception
   */
  public String getPK(String tn)
      throws SQLException,Exception
  {
    String sql="select 1+contador,codigo_descriptor "
              +"from descriptores where descriptor='"+tn+"'";
    Statement st=conn.createStatement();
    ResultSet rs=st.executeQuery(sql);
    String ret=null;
    if(rs.next())
      ret=rs.getString(1);
    else
      throw new Exception("No existe contador para la tabla "+tn);

    st.executeUpdate(
        "update descriptores set contador="
        +ret
        +" where codigo_descriptor="+rs.getString(2)
        );
    return ret;
  }

}