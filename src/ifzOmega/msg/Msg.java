package ifzOmega.msg;

import ifzOmega.msg.reg.gen;
import java.util.*;
import java.io.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

public class Msg {
  Vector regs=new Vector();

  public Msg(){
  }

  public void add( gen g ){
    regs.add(g);
  }

  public Vector getRegs(){
    return regs;
  }

  public String toString(){
    String ret="";
    for(Iterator i=regs.iterator();i.hasNext();)
      ret=ret+((gen)i.next()).toString()+"\r\n";
    return ret;
  }

  public void write(FileOutputStream fos)
  throws IOException
  {
    for(Iterator i=regs.iterator();i.hasNext();){
      String cad=((gen)i.next()).toString()+"\r\n";
      fos.write( cad.getBytes() );
    }
  }

}
