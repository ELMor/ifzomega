// $ANTLR 2.7.1: "src/ifzOmega/msg/astm.g" -> "ASTMlexer.java"$

package ifzOmega.msg;
import  ifzOmega.msg.reg.*;

public interface ASTMlexerTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int HHEA = 4;
	int NL = 5;
	int PHEA = 6;
	int OHEA = 7;
	int RHEA = 8;
	int MHEA = 9;
	int CHEA = 10;
	int LHEA = 11;
	int FIELD = 12;
	int WS = 13;
}
