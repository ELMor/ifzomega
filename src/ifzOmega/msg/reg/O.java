package ifzOmega.msg.reg;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class O extends gen {

 public static final int seqOrd= 2;
 public static final int seqPet= 3;
 public static final int testID= 5;
 public static final int actCod=12;
 public static final int codMue=16;

  public O() {
    super("O",30);
  }
}
