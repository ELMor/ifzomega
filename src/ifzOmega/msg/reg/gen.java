package ifzOmega.msg.reg;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class gen {
  int     tamano=0;
  String  cabecera=null;
  String  campos[]=null;

  public gen(String head, int numCampos) {
    campos=new String[numCampos];
    tamano=numCampos;
    cabecera=head;
  }

  public String getField(int i){
    return campos[i-2];
  }

  public void setField(int i,String val){
    if(val!=null && val.length()>0)
      if(val.charAt(0)=='|')
        val=val.substring(1);
    //System.out.println("Setting "+cabecera+" campo "+i+" valor "+val);
    campos[i-2]=val;
  }

  public String toString(){
    String ret=cabecera;
    for(int i=0;i<tamano;i++)
      ret=ret+ "|" + (campos[i]==null ? "" : campos[i]);
    return ret;
  }

  public String getHead(){
    return cabecera;
  }
}
