package ifzOmega.msg.reg;
import java.text.*;
import java.util.Date;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class H extends gen{

  public H() {
    super("H",13);
    setField( 2,"\\^&");
    setField( 5,"Soluziona");
    setField(10,"Roche");
    setField(12,"P");
    SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
    setField(14,sdf.format(new Date()));
  }

}
