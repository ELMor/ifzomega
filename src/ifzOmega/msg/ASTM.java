// $ANTLR 2.7.1: "src/ifzOmega/msg/astm.g" -> "ASTM.java"$

package ifzOmega.msg;
import  ifzOmega.msg.reg.*;

import antlr.TokenBuffer;
import antlr.TokenStreamException;
import antlr.TokenStreamIOException;
import antlr.ANTLRException;
import antlr.LLkParser;
import antlr.Token;
import antlr.TokenStream;
import antlr.RecognitionException;
import antlr.NoViableAltException;
import antlr.MismatchedTokenException;
import antlr.SemanticException;
import antlr.ParserSharedInputState;
import antlr.collections.impl.BitSet;
import antlr.collections.AST;
import antlr.ASTPair;
import antlr.collections.impl.ASTArray;

public class ASTM extends antlr.LLkParser
       implements ASTMTokenTypes
 {

protected ASTM(TokenBuffer tokenBuf, int k) {
  super(tokenBuf,k);
  tokenNames = _tokenNames;
}

public ASTM(TokenBuffer tokenBuf) {
  this(tokenBuf,2);
}

protected ASTM(TokenStream lexer, int k) {
  super(lexer,k);
  tokenNames = _tokenNames;
}

public ASTM(TokenStream lexer) {
  this(lexer,2);
}

public ASTM(ParserSharedInputState state) {
  super(state,2);
  tokenNames = _tokenNames;
}

	public final Msg  getMsg() throws RecognitionException, TokenStreamException {
		Msg m;
		
		
			m=new Msg();
		
		
		hreg(m);
		{
		int _cnt7=0;
		_loop7:
		do {
			if ((LA(1)==PHEA)) {
				preg(m);
				{
				int _cnt6=0;
				_loop6:
				do {
					if ((LA(1)==OHEA)) {
						oreg(m);
						{
						int _cnt5=0;
						_loop5:
						do {
							switch ( LA(1)) {
							case RHEA:
							{
								rreg(m);
								break;
							}
							case MHEA:
							{
								mreg(m);
								break;
							}
							case CHEA:
							{
								creg(m);
								break;
							}
							default:
							{
								if ( _cnt5>=1 ) { break _loop5; } else {throw new NoViableAltException(LT(1), getFilename());}
							}
							}
							_cnt5++;
						} while (true);
						}
					}
					else {
						if ( _cnt6>=1 ) { break _loop6; } else {throw new NoViableAltException(LT(1), getFilename());}
					}
					
					_cnt6++;
				} while (true);
				}
			}
			else {
				if ( _cnt7>=1 ) { break _loop7; } else {throw new NoViableAltException(LT(1), getFilename());}
			}
			
			_cnt7++;
		} while (true);
		}
		lreg(m);
		return m;
	}
	
	public final void hreg(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			H h=null;
		
		
		match(HHEA);
		h=new H(); m.add(h);
		fields(h);
		match(NL);
	}
	
	public final void preg(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			P p=null;
		
		
		match(PHEA);
		p=new P(); m.add(p);
		fields(p);
		match(NL);
	}
	
	public final void oreg(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			O o=null;
		
		
		match(OHEA);
		o=new O(); m.add(o);
		fields(o);
		match(NL);
	}
	
	public final void rreg(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			R r=null;
		
		
		match(RHEA);
		r=new R(); m.add(r);
		fields(r);
		match(NL);
	}
	
	public final void mreg(
		Msg msg
	) throws RecognitionException, TokenStreamException {
		
		
			M m=null;
		
		
		match(MHEA);
		m=new M(); msg.add(m);
		fields(m);
		match(NL);
	}
	
	public final void creg(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			C c=null;
			int pos=2;
		
		
		match(CHEA);
		c=new C(); m.add(c);
		fields(c);
		match(NL);
	}
	
	public final void lreg(
		Msg m
	) throws RecognitionException, TokenStreamException {
		
		
			L l=null;
		
		
		match(LHEA);
		l=new L(); m.add(l);
		fields(l);
		{
		switch ( LA(1)) {
		case NL:
		{
			match(NL);
			break;
		}
		case EOF:
		{
			match(Token.EOF_TYPE);
			break;
		}
		default:
		{
			throw new NoViableAltException(LT(1), getFilename());
		}
		}
		}
	}
	
	public final void fields(
		gen m
	) throws RecognitionException, TokenStreamException {
		
		Token  f0 = null;
		
			int pos=2;
		
		
		{
		int _cnt18=0;
		_loop18:
		do {
			if ((LA(1)==FIELD)) {
				f0 = LT(1);
				match(FIELD);
				m.setField(pos++,f0.getText());
			}
			else {
				if ( _cnt18>=1 ) { break _loop18; } else {throw new NoViableAltException(LT(1), getFilename());}
			}
			
			_cnt18++;
		} while (true);
		}
	}
	
	
	public static final String[] _tokenNames = {
		"<0>",
		"EOF",
		"<2>",
		"NULL_TREE_LOOKAHEAD",
		"HHEA",
		"NL",
		"PHEA",
		"OHEA",
		"RHEA",
		"MHEA",
		"CHEA",
		"LHEA",
		"FIELD"
	};
	
	
	}
