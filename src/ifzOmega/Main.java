package ifzOmega;

import java.sql.*;
import java.net.*;
import java.io.*;
import java.util.*;
import ifzOmega.run.dirReader;
import ifzOmega.run.dirWriter;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

/**
 * Clase principal, arranca las 2 hebras (lectura y escritura en directorios)
 * Y se encarga de revisar periodicamente su salud. Las rearranca si han muerto.
 *
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Soluziona</p>
 * @author unascribed
 * @version 1.0
 */

class Main {
  private String 			 jdbcurl;
  private int					 poolsize;
  private int					 puerto;
  private Thread   		 iReader,iWriter;
  private Runnable 		 cReader,cWriter;

  public static void main(String arg[]){
    new Main(arg);
  }

  Main(String arg[]){
    int    	opt[]; //Tiene en cuenta el par�metro opcional
    String 	dbtype,    jdbcurl,    login,    passwd,    dirw,    dirr,    inip;
    int		port,offset=0;


    //Debe haber 7 argumentos
    opt = new int[2];
    if( arg.length < 7 ){
      Instrucciones();
    }else{
      for(int na=0;na<arg.length-8;na++){
        if(arg[na].equalsIgnoreCase("-d") ){
          opt[0]=1;
          offset++;
        }else{
          System.out.println("No reconozco "+arg[na]);
          Instrucciones();
        }
      }
    }

    dbtype =arg[offset+0];
    jdbcurl=arg[offset+1];
    login  =arg[offset+2];
    passwd =arg[offset+3];
    dirr   =arg[offset+4];
    dirw   =arg[offset+5];
    inip   =arg[offset+6];

    try {
      cReader = new dirReader(opt,dbtype,jdbcurl,login,passwd,dirr);
      cWriter = new dirWriter(opt,dbtype,jdbcurl,login,passwd,dirw,inip);
    }catch(Exception e){
      e.printStackTrace();
      System.exit(-1);
    }

    //Se arranca ordenadamente las 3 hebras
    //Monitorizar los thread
    for(;;){
      try {
        if( iReader==null || !iReader.isAlive() ){
          System.out.println("Main: Thread iReader, iniciando");
          iReader = new Thread( cReader );
          iReader.start();
        }
        if( iWriter==null || !iWriter.isAlive() ){
          System.out.println("Main: Thread iWriter, iniciando");
          iWriter = new Thread ( cWriter );
          iWriter.start();
        }
        Thread.sleep(5000); //Cada 5 segundos
      }catch(InterruptedException e){
        e.printStackTrace();
      }
    }
  }

  /**
   * Muestra instrucciones en pantalla
   */
  private void Instrucciones(){
    System.out.println("\nUso: java -jar ifzOmega.jar [-d] driver url log passwd dirR dirW initPet\n");
    System.out.println("\t[-d]    : Presenta mensajes de depuracion");
    System.out.println("\tdriver  : [ oracle | sqlserver | sybase ]");
    System.out.println("\turl     : (oracle)    host:port:sid");
    System.out.println("\t          (sqlserver) host:port");
    System.out.println("\t          (sybase)    host:port");
    System.out.println("\tlog     : Usuario de BBDD");
    System.out.println("\tpasswd	: Password del usuario de BBDD");
    System.out.println("\tdirR    : Directorio del que leer los resultados de Ross.");
    System.out.println("\tdirW    : Directorio donde grabar las peticiones a Ross.");
    System.out.println("\tinitPet : Numero inicial de peticiones generadas por OpenSIC (inicio diario).\n");
    System.out.println("\nATENCION: Asegurese de haber ejecutado los scripts que acompa�an esta interfaz");
    System.out.println("En este archivo JAR se encuentran dichos scripts para MSSQLServer, extraerlos asi:");
    System.out.println("jar xvf ifzOmega.jar IO_MSSQLServer.sql");
    System.exit(-1);
  }
}