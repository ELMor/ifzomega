/* ============================================================ */
/*   Database name:  NOVAHIS                                    */
/*   DBMS name:      Microsoft SQL Server 6.x                   */
/*   Created on:     12/02/2002  11:36                          */
/* ============================================================ */

create table ifzastm
(
    actividad_det_pk         int                   not null,
    ifz_seq_omega            int                   null    ,
    ifz_seq_pet              int                   null    ,
    ifz_fec_envio            varchar(14)           null    ,
    ifz_senvio               varchar(2)            null    ,
    ifz_fec_recep            varchar(14)           null    ,
    ifz_srecep               varchar(2)            null    ,
    constraint PK_IFZASTM primary key (actividad_det_pk)
)
;

alter table ifzastm
    add constraint FK_IFZASTM_RELATION__ACTIVIDA foreign key  (actividad_det_pk)
       references actividad_det (actividad_det_pk)
;

CREATE TRIGGER ti_actividad_det  ON actividad_det 
FOR INSERT
AS
BEGIN
INSERT INTO  ifzastm  (actividad_det_pk)
SELECT actividad_det_pk
FROM inserted, actividad
where ( actividad.actest_pk is not null) and inserted.actividad_pk=actividad.actividad_pk
END
;

